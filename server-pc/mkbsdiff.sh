#!/bin/bash

qcast_VERSION="1.0.0";

print_usage()
{
	echo "$0 old-folder/old-file new-folder/new-file related-file"
}

if [[ $# != 3 ]]; then
	print_usage;
	exit 1;
fi

set -o errexit
set -o nounset

ABSDIR=$(cd ${0%/*} && pwd);
if [[ "$ABSDIR" == *" "* ]]; then
	echo "Error: Build path can't contain [space]";
	exit 1;
fi
source "$ABSDIR/../packages/version-info";

MACH=$(uname -m);
BSDIFF="$ABSDIR/bsdiff-$bsdiff_VERSION-$MACH";
if [[ ! -f "$BSDIFF" ]]; then
	BSDIFF="$ABSDIR/built/bsdiff-$bsdiff_VERSION-$MACH";
	if [[ ! -f "$BSDIFF" ]]; then
		echo "Error: Can't found bsdiff-$bsdiff_VERSION-$MACH";
		exit 1;
	fi
fi

OLD_DIR="$1";
NEW_DIR="$2";
RELATED_FILE="$3";

#if [[ -d "$OLD_DIR"  ]]; then
	#OLD_FILES=$(find "$OLD_DIR" -type f);
#elif [[ -f "$OLD_DIR" ]]; then
	#OLD_FILES="$OLD_DIR";
#else
	#echo "Error: Invalid path for old folder/file: $OLD_DIR";
	#exit 1;
#fi
#if [[ -d "$NEW_DIR"  ]]; then
	#NEW_FILES=$(find "$NEW_DIR" -type f);
#elif [[ -f "$NEW_DIR" ]]; then
	#NEW_FILES="$NEW_DIR";
#else
	#echo "Error: Invalid path for new folder/file: $NEW_DIR";
	#exit 1;
#fi
PATCH_DIR="$ABSDIR/qcast/$qcast_VERSION";

mkdiff()
{
	echo aaa;
}

mkdiff_by_libs_list()
{
	local libs_list="$ABSDIR/libs-list";
	local libs_list_fmt="$libs_list-fmt";
	grep -v "^#" "$libs_list" > "$libs_list_fmt";

	local line_num=$(sed -n "$=" $libs_list_fmt);
	if (( ! $line_num )); then
		log_error "can't found march item in $libs_list_fmt"
		return 1
	fi

	for (( idx=1; idx<=$line_num; idx++ )); do
		libinfo=$(sed -n "$idx"p $libs_list_fmt);


		TODO
		echo "$idx <:> $libinfo";
	done

	#for (( idx = 1; idx <= ${#pid[@]}; idx++ )) do
		#wait ${pid[idx]}
	#done

	return 0
}
mkdiff_by_libs_list;


exit 0;
append_file()
{
	local filename="$1";
	echo " Appending $filename ...";
	#mkdir -p 
	#$BSDIFF 

}

bsdiff_file()
{
	local filename="$1";
	echo " Making    $filename.patch ...";
	mkdir -p $PATCH_DIR/${filename%/*};
	$BSDIFF $OLD_DIR/$filename $NEW_DIR/$filename $PATCH_DIR/$filename.patch;
}

for eachfile in $NEW_FILES; do
	if [[ "$OLD_FILES" != *"$eachfile"* ]]; then
		# It's a new added file, process in addfiles
		append_file $eachfile
	elif [[ $(md5sum "$OLD_DIR/$eachfile") == $(md5sum "$NEW_DIR/$eachfile") ]]; then
		# the same file, noneed process
		continue;
	else
		# different file, process by bsdiff_file
		bsdiff_file $eachfile
	fi

done
