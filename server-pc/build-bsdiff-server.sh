#!/bin/bash

set -o errexit
set -o nounset

ABSDIR=$(cd ${0%/*} && pwd);
if [[ "$ABSDIR" == *" "* ]]; then
	echo "Error: Build path can't contain [space]";
	exit 1;
fi
source "$ABSDIR/../packages/version-info";

rm -rf "$ABSDIR"/{output,built};
mkdir "$ABSDIR"/{output,built};

unpack_packages()
{
	echo "Unpacking source code ...";
	tar zxf "$ABSDIR/../packages/bsdiff-$bsdiff_VERSION.tar.gz" -C "$ABSDIR/output";
	tar zxf "$ABSDIR/../packages/bzip2-$bzip2_VERSION.tar.gz" -C "$ABSDIR/output";
	echo "Unpacked source code.";
}

build_binary()
{
	local binary=$1
	local sources=$2

	local bin_OBJECTS=;
	local bin_PROGRAMS="$binary-$bsdiff_VERSION-$MACH";

	echo "Building $binary ...";
	for src in $sources; do
		srcname=${src##*/};
		srcname=${srcname%.c};
		obj="$OBJPATH/${binary}_${srcname}.o"

		echo " CC $srcname.c";
		$CC $src $CFLAGS -c -o $obj;
		bin_OBJECTS+=" $obj";
	done

	echo " CCLD $bin_PROGRAMS";
	$CC $bin_OBJECTS -o $ABSDIR/built/$bin_PROGRAMS;

	echo "Built $binary.";
}

MACH=$(uname -m);
CC="gcc";
CFLAGS="-I$ABSDIR/output/bzip2-$bzip2_VERSION";
OBJPATH=$ABSDIR/output/objs;
mkdir -p $OBJPATH;

bsdiff_SOURCES="$ABSDIR/output/bsdiff-$bsdiff_VERSION/bsdiff.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/blocksort.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/bzlib.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/compress.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/crctable.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/decompress.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/huffman.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/randtable.c";

bspatch_SOURCES="$ABSDIR/output/bsdiff-$bsdiff_VERSION/bspatch.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/blocksort.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/bzlib.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/compress.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/crctable.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/decompress.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/huffman.c \
				$ABSDIR/output/bzip2-$bzip2_VERSION/randtable.c";

unpack_packages;
build_binary "bsdiff" "$bsdiff_SOURCES";
build_binary "bspatch" "$bspatch_SOURCES";

echo "Done !!!";
