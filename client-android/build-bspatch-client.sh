#!/bin/bash

print_usage()
{
	echo "$0 ndk_path"
}

if [[ $# != 1 ]]; then
	print_usage;
	exit 1;
elif [[ ! -f "$1/ndk-build" ]]; then
	echo "Error: Uncorrect ndk path."
	print_usage;
	exit 1;
fi

set -o errexit
set -o nounset

ABSDIR=$(cd ${0%/*} && pwd);
if [[ "$ABSDIR" == *" "* ]]; then
	echo "Error: Build path can't contain [space]";
	exit 1;
fi
NDK_BUILD="$1/ndk-build";

rm -rf "$ABSDIR"/{jni,obj};
mkdir "$ABSDIR/jni";

unpack_packages()
{
	echo "Unpacking source code ...";
	source "$ABSDIR/../packages/version-info";
	tar zxf "$ABSDIR/../packages/bsdiff-$bsdiff_VERSION.tar.gz" -C "$ABSDIR/jni";
	tar zxf "$ABSDIR/../packages/bzip2-$bzip2_VERSION.tar.gz" -C "$ABSDIR/jni";
	echo "Unpacked source code.";
}

create_makefile()
{
	echo 'Creating Android.mk'
	#> $ABSDIR/jni/Android.mk # Create new config file
	cat << EOF > $ABSDIR/jni/Android.mk
LOCAL_PATH := \$(call my-dir)
include \$(CLEAR_VARS)

LOCAL_SRC_FILES :=  bsdiff-$bsdiff_VERSION/bspatch.c \\
					bzip2-$bzip2_VERSION/bzlib.c \\
					bzip2-$bzip2_VERSION/crctable.c \\
					bzip2-$bzip2_VERSION/randtable.c \\
					bzip2-$bzip2_VERSION/decompress.c \\
					bzip2-$bzip2_VERSION/huffman.c
LOCAL_MODULE := bspatch
LOCAL_C_INCLUDES := \$(LOCAL_PATH)/bzip2-$bzip2_VERSION
include \$(BUILD_EXECUTABLE)
EOF
}

build_binary()
{
	$NDK_BUILD NDK_PROJECT_PATH=$ABSDIR
}

unpack_packages;
create_makefile;
build_binary;
