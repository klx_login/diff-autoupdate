package com.demo.autoupdate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.io.InputStream;
import java.io.FileOutputStream;
import javax.xml.xpath.XPath;

public class AutoUpdate {
	public enum ErrType {
		NoError,
		IO,
		Interrupted,
		MalformedURL,
		
		//private ErrNo(int errno) {
			//this.errno = errno;
		//}
		//private int errno;
	}
	
	public static AutoUpdate getInstance() {
		return AutoUpdateHolder.instance;
	}

	public interface Notify {
	}
	public void setNotifyCallback(/* TODO */) {
	}

	public void setDirs(final String dataDir, final String extDir) {
		data_dir = dataDir;
		ext_dir = extDir;
	}

	public boolean runAsync() {
		// 0. TODO start a new thread
		// 1. TODO download update-info.json
		// 2. TODO check Current Version
		// 3. TODO download corresponding update-info-vX.X.json
		// 4. TODO parse json
		// 5. TODO download all new diff packages
		// 6. TODO patch diff file to new
		// 7. TODO mv new packages to install folder
		// 8. TODO clear cache
		
		ErrType retval;

		//retval = download();
		//if ( retval != ErrType.NoError ) {
		//}
		//https://bitbucket.org/klx_login/android-autoupdate/downloads/bspatch-4.3.8-armv7

		retval = doPatch("/data/data/com.demo.autoupdate/cache/bspatch",
						 "/sdcard/test-1.txt",
						 "/sdcard/aaa.patch", 
						 "/data/data/com.demo.autoupdate/cache/bbb");
		if ( retval != ErrType.NoError ) {
		}

		retval = checkMD5("/sdcard/bsdiff/tmp/oldfile", "1234567890");
		if ( retval != ErrType.NoError ) {
		}
		
		return true;
	}

	public void stop() {
	}
	
	private AutoUpdate() {
	}

	private static class AutoUpdateHolder{
		private static AutoUpdate instance = new AutoUpdate();
	}

	private ErrType download(String urlstr, String localname) {
    	try {
			URL url = new URL(urlstr);
			HttpURLConnection urlconn = (HttpURLConnection)url.openConnection();
			urlconn.setRequestMethod("GET");
			urlconn.setInstanceFollowRedirects(true);
			urlconn.connect();
			InputStream in = urlconn.getInputStream();
			FileOutputStream out = new FileOutputStream(localname);
			int read;
			byte[] buffer = new byte[4096];
			while ((read = in.read(buffer)) > 0) {
				out.write(buffer, 0, read);
			}
			out.close();
			in.close();
			urlconn.disconnect();
		} catch (MalformedURLException e) {
			//throw new RuntimeException(e);
			return ErrType.MalformedURL;
		} catch (IOException e) {
			//throw new RuntimeException(e);
			return ErrType.IO;
		}
    	
    	return ErrType.NoError;
    }

	private ErrType doPatch(String patchcmd, String oldfile, String patchfile, String newfile) {
		String cmdline = patchcmd + " " + oldfile + " " + newfile + " " +patchfile;

		try {
			Process process = Runtime.getRuntime().exec(cmdline);
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			int read;
			//char[] buffer = new char[4096];
			char[] buffer = new char[256];
			StringBuffer output = new StringBuffer();
			while ((read = reader.read(buffer)) > 0) {
				output.append(buffer, 0, read);
			}
			reader.close();
			process.waitFor();
			//return output.toString();
		} catch (IOException e) {
			//throw new RuntimeException(e);
			return ErrType.IO;
		} catch (InterruptedException e) {
			//throw new RuntimeException(e);
			return ErrType.Interrupted;
		}

		return ErrType.NoError;
	}

	private ErrType checkMD5(String filename, String md5value) {

		return ErrType.NoError;
	}

	final String tag = "AutoUpdate";
	private String ext_dir = null;
	private String data_dir = null;
}
